import frappe

#send_email for meeting invitation
@frappe.whitelist()
def send_invitation_emails(Meeting):
	meeting=frappe.get_doc("Meeting",Meeting)
	meeting.check_permission("email")
	if meeting.status=="Planned":
		frappe.sendmail(
			recipients=[d.attendee for d in meeting.attendees],
			sender=frappe.session.user,
			subject=meeting.title,
			message=meeting.invitation_message,
			reference_doctype=meeting.doctype,
			reference_name=meeting.name,
		)
		meeting.status="Invitation Send"
		meeting.save()
	else:
		frappe.msgprint("Meeting Status Must Be Planned")

# Display meetings on calendar
@frappe.whitelist()
def get_meetings(start,end):
 	if not frappe.has_permission("Meeting","read"):
		raise frappe.PermissionError

	return frappe.db.sql("""select
		timestamp('date',fromtime) as start,
		timestamp('date',totime) as end,
		name,
		title,
		status
		from tabMeeting
		where date between %(start)s and %(end)s""",{
		"start":start,
		"end":end
		},as_dict=True,debug=1)

# For sending meeting minutes
@frappe.whitelist()
def send_minutes_emails(assigned_to,action,description):
	frappe.sendmail(
		recipients=assigned_to,
		sender=frappe.session.user,
		subject=action,
		message=description,
		)
	print"***************************** send"



