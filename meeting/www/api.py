import frappe

@frappe.whitelist()
def send_invitation_emails(meeting):

	meeting=frappe.get_doc("meeting",meeting)
	meeting.check_permission("email")

	if meeting.status=="planned":
		frapee.sendmail(
			recipients=[d.attendee for d in meeting.attendee],
			sender=frappe.session.user,
			subject=meeting.title,
			message=meeting.invitation_message,
			reference_doctype=meeting.doctype,
			reference_name=meeting.name,
			as_bulk=True
		)
		meeting.status="Invitation Sent"
		meeting.save()
	else:
		frappe.msgprint("Meeting Statud Must Be Planned")
	