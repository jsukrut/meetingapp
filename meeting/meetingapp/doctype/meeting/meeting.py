# -*- coding: utf-8 -*-
# Copyright (c) 2015, jsukrut and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import frappe.utils

class Meeting(Document):
	def validate(self):
		pass
		# for attendee in self.attendees:
		# 	if not attendee.full_name:
		# 		attendee.full_name=get_full_name(attendee.attendee)

@frappe.whitelist()
def get_full_name(attendee):
	user=frappe.get_doc("User",attendee)

	return "".join(filter(None,{user.first_name,user.middle_name,user.last_name}))

@frappe.whitelist()
def check_status(name):
	meeting=frappe.get_doc("Meeting",name)
	meeting_date=meeting.date
	meeting_fromtime=meeting.fromtime
	
	cur_date=frappe.utils.data.nowdate()
	cur_time=frappe.utils.data.nowtime()
	#frappe.errprint(cur_time)
	#frappe.errprint(meeting_fromtime)
	#diff=frappe.utils.data.date_diff(cur_date,meeting_date)
 	#frappe.errprint(diff) 
	#if diff>0:
	time_diff=frappe.utils.data.time_diff(cur_time,meeting_fromtime) 
	time_diff=frappe.utils.data.get_time (time_diff)
	hour=int(time_diff.hour)
	if hour==0:
		minute=int(time_diff.minute)
		if minute>30:
			meeting.status="Deleted"
			meeting.save()
	else:
		meeting.status="Deleted"
		meeting.save()


			 

	  
