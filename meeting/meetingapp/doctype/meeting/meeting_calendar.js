frappe.views.calendar["Meeting"] = {
	field_map: {
		"start": "fromtime",
		"end": "totime",
		"id": "name",
		"status": "status",
	},
	
	get_events_method: "meeting.api.get_meetings"
}