frappe.views.calendar["Meeting"] = {
	field_map: {
		"start": "fromtime",
		"end": "totime",
		"id": "name",
		"allDay": "all_day",
		"title": "subject",
		"status": "event_type",
	},
	style_map: {
		"Public": "success",
		"Private": "info"
	},
	get_events_method: "meeting.api.get_meetings"