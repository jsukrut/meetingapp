// Copyright (c) 2016, jsukrut and contributors
// For license information, please see license.txt

frappe.ui.form.on("Meeting Attendee",{
	attendee:function(frm,cdt,cdn){
		var attendee=frappe.model.get_doc(cdt,cdn);
		if(attendee.attendee){
			 frappe.call({
			 	method:"meeting.meetingapp.doctype.meeting.meeting.get_full_name",
			 	args:{
			 		attendee:attendee.attendee
			 	},
			 	callback:function(r){
			 		frappe.model.set_value(cdt,cdn,"full_name",r.message);
			 	}
			 });

		}
		/*else{
			frappe.model.set_value(cdt,cdn,"full_name",null)
		}*/
	}
})

frappe.ui.form.on('Meeting', {
	onload: function(frm) {
		frappe.call({
				method:"meeting.meetingapp.doctype.meeting.meeting.check_status",
				args:{
					 name:frm.doc.name
				}
			   
			});	
	},


	totime: function(frm) {
		var from_time=frm.doc.fromtime;
		if(frm.doc.totime<=from_time)
		{
			frappe.msgprint(" To Time Must Be Greater Than From Time");
		}
	},

	date:function(frm){
		var a=frappe.datetime.get_diff(Date.parse(frm.doc.date),frappe.datetime.nowdate());
        if(a<=-1)
        {
        	frappe.msgprint("Check Meeting Date");
        }


	},
	validate:function(frm){
		
		
	},	

	fromtime:function(frm){
		if(frm.doc.fromtime>=frm.doc.totime)
		{
			frappe.throw(" From Time Must Be Smaller Than To Time");
		}



	},

	send_emails:function(frm){
		if(frm.doc.status=="Planned")
		{
			frappe.call({
				method:"meeting.api.send_invitation_emails",
				args:{
					Meeting:frm.doc.name
				}
			   
			});	
		}
	}, 
	send_minutes:function(frm){
		var l=frm.doc.minutes.length
		var i=0
		for (i=0;i<l;i++){
		 	frappe.call({
		 		method:"meeting.api.send_minutes_emails",
		 		args:{
		 			assigned_to:frm.doc.minutes[i]['assigned_to'],
		 			action:frm.doc.minutes[i]['action'],
		 			description:frm.doc.minutes[i]['description']+"Date of complition:"+frm.doc.minutes[i]['complete_by'],
		 		}
		 	});
		 	console.log("DONE");
		 	console.log(frm.doc.minutes[i]['assigned_to']);
		 	console.log(frm.doc.minutes[i]['action']);
		 	console.log(frm.doc.minutes[i]['description']+"Date of complition:"+frm.doc.minutes[i]['complete_by'])
		}
		 	
	}


});




